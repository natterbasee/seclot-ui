import {
  organizationApi,
  generalAuthApi,
  errorHandler,
  adminApi,
  generalApi,
} from './index'
import store, {
  SAVE_PROFILE_MUTATION,
  SAVE_TOKEN_MUTATION,
  ACCOUNT_ID_MUTATION,
} from '../store'
import firebase from 'firebase/app'
import 'firebase/auth'

export default {
  login(email, password) {
    return organizationApi
      .post('/login', {email, password})
      .then((response) => completeSignIn(email, password, response.data))
      .catch(errorHandler)
  },

  confirm(token) {
    return organizationApi
      .get('/confirm-account/' + token)
      .then((response) => response.data)
      .catch(errorHandler)
  },

  adminLogin(email, password) {
    return adminApi
      .post('/login', {email, password})
      .then((response) => completeSignIn(email, password, response.data))
      .catch(errorHandler)
  },

  register(name, email, phoneNumber, location, password) {
    return organizationApi
      .post('/register', {name, email, phoneNumber, location, password})
      .then((response) => response.data)
      .catch(errorHandler)
  },

  requestPasswordReset(email) {
    return generalAuthApi
      .get(`/request-password-reset?email=${email}`)
      .then((response) => response.data.message)
      .catch(errorHandler)
  },

  adminRequestPasswordReset(email) {
    return generalApi
      .post(`/admin/request-reset-password`, {email})
      .then((response) => response.data.message)
      .catch(errorHandler)
  },

  resetPassword(code, password) {
    return generalAuthApi
      .post('reset-password', {code, password})
      .then((response) => response.data.message)
      .catch(errorHandler)
  },

  adminResetPassword(password, token) {
    return generalApi
      .post('/admin/reset-password', {password, token})
      .then((response) => response.data.message)
      .catch(errorHandler)
  },

  refreshToken(refreshToken) {
    return generalAuthApi
      .post('/refresh-token', {refreshToken})
      .then((response) => response.data)
  },
}

function completeSignIn(email, password, authResponse) {
  return firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then((firebaseUser) => {
      store.commit(SAVE_TOKEN_MUTATION, authResponse)
      store.commit(SAVE_PROFILE_MUTATION, authResponse.profile)
      store.commit(ACCOUNT_ID_MUTATION, firebaseUser.user.uid)
      return authResponse
    })
}
