import { adminApi, errorHandler } from '../index';

export function sendMessage(messageBody) {
  return adminApi
    .post('/message', messageBody)
    .then(response => response.data.recipientsCount)
    .catch(errorHandler);
}
