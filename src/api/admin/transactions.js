import { adminApi, errorHandler } from '../index';

export default {
  fetch() {
    return adminApi
      .get('/report/transactions')
      .then(response => response.data)
      .catch(errorHandler);
  }
};
