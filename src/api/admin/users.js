import { adminApi, errorHandler } from '../index';

export default {
  fetch() {
    return adminApi
      .get('/report/users')
      .then(response => response.data)
      .catch(errorHandler);
  },

  suspendUser(id) {
    return adminApi
      .post(`/suspend-user/${id}`)
      .then(response => response.data)
      .catch(errorHandler);
  },

  activateUser(id) {
    return adminApi
      .post(`/reactivate-user/${id}`)
      .then(response => response.data)
      .catch(errorHandler);
  }
};
