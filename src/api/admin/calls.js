import { adminApi, errorHandler } from '../index';

export default {
  fetch() {
    return adminApi
      .get('/report/distress-calls')
      .then(response => response.data)
      .catch(errorHandler);
  },

  updateCallStatus(payload) {
    return adminApi
      .patch('/report/distress-calls', payload)
      .then(response => response.data)
      .catch(errorHandler);
  }
};
