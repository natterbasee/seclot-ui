import { adminApi, errorHandler, generalApi } from '../index';

export default {
  fetch() {
    return adminApi
      .get('/report/organizations')
      .then(response => response.data)
      .catch(errorHandler);
  },

  sectors() {
    return generalApi
      .get('/org-sectors')
      .then(response => response.data)
      .catch(errorHandler);
  },

  addOrganization(payload) {
    return adminApi
      .post('/org', payload)
      .then(response => response.data)
      .catch(errorHandler);
  },

  addSectors(payload) {
    return adminApi
      .post('/org-sectors', { sectors: payload })
      .then(response => response.data)
      .catch(errorHandler);
  },

  deleteSector(payload) {
    return adminApi
      .delete(`/org-sectors?sectors=${payload}`)
      .then(response => response.data)
      .catch(errorHandler);
  }
};
