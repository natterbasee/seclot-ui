import { adminApi, generalApi, errorHandler } from '../index';

export default {
  fetch() {
    return generalApi
      .get('/config')
      .then(response => response.data)
      .catch(errorHandler);
  },

  updateSubscription(payload) {
    return adminApi
      .put('/config', payload)
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },
  resetSubscription() {
    return adminApi
      .delete('/config?fields=subscriptionPlans')
      .then(response => response.data)
      .catch(errorHandler);
  }
};
