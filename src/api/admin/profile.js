import { adminApi, errorHandler } from '../index';
import store, { SAVE_PROFILE_MUTATION } from '../../store';

export default {
  get() {
    return adminApi
      .get('/profile')
      .then(response => {
        store.commit(SAVE_PROFILE_MUTATION, response.data);
        return response.data;
      })
      .catch(errorHandler);
  },

  update(payload) {
    return adminApi
      .put('/profile', payload)
      .then(response => {
        store.commit(SAVE_PROFILE_MUTATION, response.data);
        return response.data;
      })
      .catch(errorHandler);
  },

  updateProfilePicture(imageFile) {
    const formData = new FormData();
    formData.append('image', imageFile);

    return adminApi
      .put('/profile/picture', formData, {
        headers: { 'Content-Type': 'multipart/form-data' }
      })
      .then(response => {
        store.commit(SAVE_PROFILE_MUTATION, response.data);
        return response.data;
      })
      .catch(errorHandler);
  },

  changePassword(payload) {
    return adminApi
      .post('/change-password', payload)
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  }
};
