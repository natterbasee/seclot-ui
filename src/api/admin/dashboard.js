import { adminApi, errorHandler } from '../index';

export default {
  fetch() {
    return adminApi
      .get('/report/dashboard')
      .then(response => response.data)
      .catch(errorHandler);
  }
};
