import { adminApi, errorHandler } from '../index';

export default {
  fetch() {
    return adminApi
      .get('/report/admins')
      .then(response => response.data)
      .catch(errorHandler);
  },

  suspendAdmin(id) {
    return adminApi
      .post(`/suspend-admin/${id}`)
      .then(response => response.data)
      .catch(errorHandler);
  },

  activateAdmin(id) {
    return adminApi
      .post(`/reactivate-admin/${id}`)
      .then(response => response.data)
      .catch(errorHandler);
  },

  addAdministrator(payload) {
    return adminApi
      .post('/add-admin', payload)
      .then(response => response)
      .catch(errorHandler);
  }
};
