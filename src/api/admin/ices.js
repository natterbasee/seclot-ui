import { adminApi, errorHandler } from "../index";

export default {
  get() {
    return adminApi
      .get("/report/ice")
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  add(name, phoneNumbers) {
    return adminApi
      .post("/report/ice", { name, phoneNumbers })
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  update(id, name, phoneNumbers) {
    return adminApi
      .put(`/report/ice/${id}`, { name, phoneNumbers })
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  delete(id) {
    return adminApi
      .delete(`/report/ice/${id}`)
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  pauseICE(id) {
    return adminApi
      .patch(`/ice/pause/${id}`)
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  unPauseICE(id) {
    return adminApi
      .patch(`/ice/unpause/${id}`)
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  getICES(id) {
    return adminApi
      .get(`/ice/get-user-ices/${id}`)
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  }
};
