import axios from 'axios';
import {
  handleExpiredTokenIssues,
  insertTokenWithRequest
} from './token-management';

export const organizationApi = axios.create({
  baseURL: `${process.env.VUE_APP_API_BASE_URL}/org`
});
handleExpiredTokenIssues(organizationApi.interceptors.response);
insertTokenWithRequest(organizationApi.interceptors.request);

export const adminApi = axios.create({
  baseURL: `${process.env.VUE_APP_API_BASE_URL}/admin`
});
handleExpiredTokenIssues(adminApi.interceptors.response);
insertTokenWithRequest(adminApi.interceptors.request);

export const generalAuthApi = axios.create({
  baseURL: `${process.env.VUE_APP_API_BASE_URL}/auth`
});

export const generalApi = axios.create({
  baseURL: `${process.env.VUE_APP_API_BASE_URL}`
});

export const errorHandler = error => {
  let errorMessage;

  if (error.response) {
    // The request was made and the server responded with a status code that falls out of the range of 2xx
    errorMessage = error.response.data.message || error.response.data;
  } else if (error.request) {
    errorMessage =
      'Error receiving response from server. Either your network is down or server is down or busy';
  } else {
    // Something happened in setting up the request that triggered an Error
    errorMessage = 'There was an error sending your request';
  }

  return Promise.reject(errorMessage);
};

export const blobErrorHandler = error => {
  let errorMessage;

  if (error.response) {
    return handleBlobErrorResponse(error.response);
  } else if (error.request) {
    errorMessage =
      'Error receiving response from server. Either your network is down or server is down or busy';
  } else {
    // Something happened in setting up the request that triggered an Error
    errorMessage = 'There was an error sending your request';
  }

  return Promise.reject(errorMessage);
};

function handleBlobErrorResponse(response) {
  return blobToString(response.data).then(errorString => {
    // check if errorString is json object
    try {
      const error = JSON.parse(errorString);
      return Promise.reject(error.message);
    } catch (e) {
      return Promise.reject(errorString);
    }
  });
}

function blobToString(blob) {
  const fileReader = new FileReader();

  return new Promise((resolve, reject) => {
    fileReader.onerror = () => {
      fileReader.abort();
      reject('Cannot read server error response');
    };

    fileReader.onload = () => {
      resolve(fileReader.result);
    };

    fileReader.readAsText(blob);
  });
}
