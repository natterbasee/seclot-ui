import { organizationApi, errorHandler, blobErrorHandler } from './index';
const FileDownload = require('js-file-download');

export default {
  get() {
    return organizationApi
      .get('/ice')
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  add(name, phoneNumbers) {
    return organizationApi
      .post('/ice', { name, phoneNumbers })
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  update(id, name, phoneNumbers) {
    return organizationApi
      .put(`/ice/${id}`, { name, phoneNumbers })
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  delete(id) {
    return organizationApi
      .delete(`/ice/${id}`)
      .then(response => {
        return response.data;
      })
      .catch(errorHandler);
  },

  pause(id) {
    return organizationApi
      .patch(`/ice/pause/${id}`)
      .then(response => response.data)
      .catch(errorHandler);
  },

  unpause(id) {
    return organizationApi
      .patch(`/ice/unpause/${id}`)
      .then(response => response.data)
      .catch(errorHandler);
  },

  downloadReport() {
    return organizationApi
      .get('/report/ice', { responseType: 'blob' })
      .then(response => {
        FileDownload(response.data, 'ice-report.pdf');
      })
      .catch(blobErrorHandler);
  }
};
