import { organizationApi, errorHandler } from "./index";

export function sendMessage(messageBody) {
  return organizationApi.post('/message', messageBody)
      .then(response => response.data.recipientsCount)
      .catch(errorHandler);
}