import axios from 'axios';
import store, { REFRESH_TOKEN_ACTION } from '../store'

const tokenErrorMessages = [
  "Authorization token not provided",
  "Unauthorized access. Token invalid or expired"
];
let isAlreadyFetchingAccessToken = false;
let retryRequestWithTokenSubscribers = [];

function retryRequestWithNewToken(accessToken) {
  retryRequestWithTokenSubscribers = retryRequestWithTokenSubscribers.filter(retryRequestCallback => retryRequestCallback.retry(accessToken))
}

function canelQueuedRequests() {
  retryRequestWithTokenSubscribers = retryRequestWithTokenSubscribers.filter(retryRequestCallback => retryRequestCallback.cancel())
}

function addSubscriber(retryRequestCallback, cancelRequestCallback) {
  retryRequestWithTokenSubscribers.push({
    retry: retryRequestCallback,
    cancel: cancelRequestCallback
  })
}

function refreshTokenAsync(refreshToken) {
  isAlreadyFetchingAccessToken = true;
  store.dispatch(REFRESH_TOKEN_ACTION, refreshToken)
      .then(retryRequestWithNewToken)
      .catch(canelQueuedRequests)
      .then(() => isAlreadyFetchingAccessToken = false)
}

export function handleExpiredTokenIssues(responseInterceptor) {
  responseInterceptor.use(response => response, error => {
    const { config, response: { status, data: { message } } } = error;
    const originalRequest = config;

    if (status !== 401 || tokenErrorMessages.indexOf(message) < 0) {
      // this is not a token related error
      return Promise.reject(error)
    }

    // token related error but can't find refreshToken to continue
    const refreshToken = store.state.refreshToken;
    if (!refreshToken || refreshToken === '') {
      return Promise.reject(error)
    }

    // token related error, try to refresh token
    if (!isAlreadyFetchingAccessToken) {
      refreshTokenAsync(refreshToken)
    }

    // queue retry for after token is refreshed
    return new Promise((resolve, reject) => {
      const retryAfterTokenRefresh = (accessToken) => {
        originalRequest.headers.token = accessToken;
        resolve(axios(originalRequest))
      };

      // update error message to be more descriptive
      error.response.data.message = 'Session timeout. Please logout and login again';
      const cancelRequestIfRefreshTokenFailed = () => reject(error);

      addSubscriber(retryAfterTokenRefresh, cancelRequestIfRefreshTokenFailed)
    })
  });
}

export function insertTokenWithRequest(requestInterceptor) {
  requestInterceptor.use(config => {
    const token = store.state.token;
    if (token) {
      config.headers.token = token;
    }
    return config;
  })
}
