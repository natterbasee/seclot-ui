import { generalApi, organizationApi, errorHandler, blobErrorHandler } from "./index";
const FileDownload = require('js-file-download');

export default {
  getPlans() {
    return generalApi.get('/config')
        .then(response => {
          const plans = response.data.subscriptionPlans;
          const planNames = Object.keys(plans);

          return planNames.map(planName => {
            return {
              name: plans[planName].name,
              amount: plans[planName].amount.formatAmount(),
              frequency: planName
            }
          })
        })
        .catch(errorHandler);
  },

  activateSubscriptionPlan(plan) {
    return organizationApi.put('/profile/subscription', { plan })
        .then(response => response.data)
        .catch(errorHandler);
  },

  topUpWallet(transactionReference) {
    return organizationApi.post('/fund-wallet', { transactionReference })
        .then(response => response.data)
        .catch(errorHandler);
  },

  getTransactionHistory() {
    return organizationApi.get('/report/wallet')
        .then(response => response.data)
        .catch(errorHandler)
  },

  downloadReport() {
    return organizationApi.get('/report/wallet/pdf', { responseType: 'blob' })
        .then(response => {
          FileDownload(response.data, 'wallet-report.pdf');
        })
        .catch(blobErrorHandler);
  }
}