import { organizationApi, errorHandler } from "./index";

export default {
  fetch() {
    return organizationApi.get('/report/dashboard')
        .then(response => response.data)
        .catch(errorHandler);
  }
}