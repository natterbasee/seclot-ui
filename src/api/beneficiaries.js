import { organizationApi, errorHandler, blobErrorHandler } from "./index";
const FileDownload = require('js-file-download');

export default {
  get() {
    return organizationApi.get('/beneficiaries')
        .then(response => {
          return response.data
        })
        .catch(errorHandler);
  },

  add(beneficiaries) {
    return organizationApi.post('/beneficiaries', { beneficiaries })
        .then(response => {
          return response.data
        })
        .catch(errorHandler);
  },

  update(beneficiaries) {
    return organizationApi.put('/beneficiaries', { beneficiaries })
        .then(response => {
          return response.data
        })
        .catch(errorHandler);
  },

  enable(beneficiaries) {
    return organizationApi.patch('/beneficiaries', { action: 'enable', phoneNumbers: beneficiaries.map(b => b.phoneNumber) })
        .then(response => {
          return response.data
        })
        .catch(errorHandler);
  },

  disable(beneficiaries) {
    return organizationApi.patch('/beneficiaries', { action: 'disable', phoneNumbers: beneficiaries.map(b => b.phoneNumber) })
        .then(response => {
          return response.data
        })
        .catch(errorHandler);
  },

  downloadReport() {
    return organizationApi.get('/report/beneficiaries', { responseType: 'blob' })
        .then(response => {
          FileDownload(response.data, 'beneficiary-report.pdf');
        })
        .catch(blobErrorHandler);
  }
}