import { organizationApi, errorHandler } from "./index";
import store, { FCM_SETUP_COMPLETE_MUTATION, SAVE_PROFILE_MUTATION } from "../store"

export default {
  get() {
    return organizationApi.get('/profile')
        .then(response => {
          store.commit(SAVE_PROFILE_MUTATION, response.data);
          return response.data
        })
        .catch(errorHandler);
  },

  update(profile) {
    return organizationApi.put('/profile', profile)
        .then(response => {
          store.commit(SAVE_PROFILE_MUTATION, response.data);
          return response.data
        })
        .catch(errorHandler);
  },

  updateProfilePicture(imageFile) {
    const formData = new FormData();
    formData.append("image", imageFile);

    return organizationApi.put('/profile/picture', formData, { headers: { 'Content-Type': 'multipart/form-data' } })
        .then(response => {
          store.commit(SAVE_PROFILE_MUTATION, response.data);
          return response.data;
        })
        .catch(errorHandler);
  },

  updateNotificationId(notificationId) {
    // don't return promise, handle everything in here
    organizationApi.put(`/notification-id`, { notificationId })
        .then(() => store.commit(FCM_SETUP_COMPLETE_MUTATION, true))
        .catch(errorHandler)
        .catch(errorMessage => {
          // eslint-disable-next-line no-console
          console.log('Error saving fcm token', errorMessage)
        });
  }
}