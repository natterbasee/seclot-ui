import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
import authentication from "../api/authentication";

Vue.use(Vuex);

export const REFRESH_TOKEN_ACTION = "refreshTokenAsync";
export const SAVE_TOKEN_MUTATION = "saveToken";
export const SAVE_PROFILE_MUTATION = "saveProfile";
export const ACCOUNT_STATUS_NOTIFICATION_MUTATION = "setAccountStatusNotice";
export const FCM_SETUP_COMPLETE_MUTATION = "updateFCMSetupStatus";
export const ACCOUNT_ID_MUTATION = "updateAccountID";
export const NOTIFICATIONS_COUNTER_MUTATION = "updateNotificationsCounter";

const vuexPersist = new VuexPersist({
  key: "seclot",
  storage: localStorage
});

const store = new Vuex.Store({
  state: {
    token: "",
    refreshToken: "",
    profile: {},
    accountStatusNotice: "",
    accountId: undefined,
    fcmSetupStatus: undefined,
    notificationsCounter: 0
  },
  mutations: {
    [SAVE_TOKEN_MUTATION](state, payload) {
      state.token = payload.token;
      state.refreshToken = payload.refreshToken;
    },
    [SAVE_PROFILE_MUTATION](state, profile) {
      state.profile = profile;
    },
    [ACCOUNT_STATUS_NOTIFICATION_MUTATION](state, status) {
      state.accountStatusNotice = status;
    },
    [FCM_SETUP_COMPLETE_MUTATION](state, status) {
      state.fcmSetupStatus = status;
    },
    [ACCOUNT_ID_MUTATION](state, accountId) {
      state.accountId = accountId;
    },
    [NOTIFICATIONS_COUNTER_MUTATION](state, value) {
      state.notificationsCounter = value;
    }
  },
  actions: {
    [REFRESH_TOKEN_ACTION]({ commit, state }) {
      return authentication.refreshToken(state.refreshToken).then(data => {
        commit(SAVE_TOKEN_MUTATION, data);
        return data.token;
      });
    }
  },
  plugins: [vuexPersist.plugin]
});

export default store;
