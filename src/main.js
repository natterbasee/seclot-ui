import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import JsonCSV from 'vue-json-csv';
import store from './store';
import router from './router';
import App from './app';

Vue.use(Buefy);
Vue.component('downloadCsv', JsonCSV);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app');
