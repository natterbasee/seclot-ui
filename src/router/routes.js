import LoginPage from '../pages/login'
import ConfirmPage from '../pages/confirmAccount'
import RegisterPage from '../pages/register'
import ForgotPasswordPage from '../pages/forgot-password'
import AdminForgotPasswordPage from '../pages/admin/forgot-password'
import ResetPasswordPage from '../pages/reset-password'
import AdminResetPasswordPage from '../pages/admin/reset-password'

import DashboardPage from '../pages/dashboard'
import ICEsPage from '../pages/ices'
import BeneficiariesPage from '../pages/beneficiaries'
import WalletPage from '../pages/wallet'
import ProfilePage from '../pages/profile'
import MessagingPage from '../pages/messaging'
import NotificationsPage from '../pages/notifications'

// import admin pages
import AdminLogin from '../pages/admin/Login'
import AdminDashboardPage from '../pages/admin/dashboard'
import AdminICEsPage from '../pages/admin/ices'
import AdminProfilePage from '../pages/admin/profile'
import AdminMessagingPage from '../pages/admin/messaging'
import OrganizationsPage from '../pages/admin/organizations'
import SubscriptionPage from '../pages/admin/subscription'
import TransactionsPage from '../pages/admin/transactions'
import UsersPage from '../pages/admin/users'
import CallHistoryPage from '../pages/admin/calls'
import MyAdminsPage from '../pages/admin/my-admins'

export const publicRoutes = [
  {
    path: '/login',
    component: LoginPage,
  },
  {
    path: '/confirm-account',
    component: ConfirmPage,
  },
  {
    path: '/register',
    component: RegisterPage,
  },
  {
    path: '/forgot-password',
    component: ForgotPasswordPage,
  },
  {
    path: '/reset-password',
    component: ResetPasswordPage,
  },
  // admin routes
  {
    path: '/admin/login',
    component: AdminLogin,
  },
  {
    path: '/admin/forgot-password',
    component: AdminForgotPasswordPage,
  },
  {
    path: '/admin/reset-password',
    component: AdminResetPasswordPage,
  },
]

export const dashboardRoutes = [
  {
    path: '/dashboard',
    component: DashboardPage,
  },
  {
    path: '/ices',
    component: ICEsPage,
  },
  {
    path: '/beneficiaries',
    component: BeneficiariesPage,
  },
  {
    path: '/wallet',
    component: WalletPage,
  },
  {
    path: '/profile',
    component: ProfilePage,
  },
  {
    path: '/messaging',
    component: MessagingPage,
  },
  {
    path: '/notifications',
    component: NotificationsPage,
  },
]

export const adminDashboardRoutes = [
  {
    path: '/admin/dashboard',
    component: AdminDashboardPage,
  },
  {
    path: '/admin/messaging',
    component: AdminMessagingPage,
  },
  {
    path: '/admin/users',
    component: UsersPage,
  },
  {
    path: '/admin/organisations',
    component: OrganizationsPage,
  },
  {
    path: '/admin/ices',
    component: AdminICEsPage,
  },
  {
    path: '/admin/transactions',
    component: TransactionsPage,
  },
  {
    path: '/admin/calls',
    component: CallHistoryPage,
  },
  {
    path: '/admin/subscription',
    component: SubscriptionPage,
  },
  {
    path: '/admin/profile',
    component: AdminProfilePage,
  },
  {
    path: '/admin/my-admins',
    component: MyAdminsPage,
  },
]
