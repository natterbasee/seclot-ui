import Vue from 'vue';
import Router from 'vue-router';

import {
  pubicRoutesGuard,
  dashboardRoutesGuard,
  adminRoutesGuard
} from './route-gaurd';
import { publicRoutes, dashboardRoutes, adminDashboardRoutes } from './routes';
import DashboardLayout from '../components/dashboard-layout';
import AdminDashboardLayout from '../components/admin-dashboard-layout';
import _404Page from '../pages/404';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/dashboard'
    },
    {
      path: '/',
      component: { template: `<router-view></router-view>` },
      children: publicRoutes,
      beforeEnter: pubicRoutesGuard
    },
    {
      path: '/',
      component: DashboardLayout,
      children: dashboardRoutes,
      beforeEnter: dashboardRoutesGuard
    },
    {
      path: '/admin',
      component: AdminDashboardLayout,
      children: adminDashboardRoutes,
      beforeEnter: adminRoutesGuard
    },
    {
      path: '*',
      component: _404Page
    }
  ]
});

export default router;
