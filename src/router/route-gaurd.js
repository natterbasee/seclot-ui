import store from '../store';

export const dashboardRoutesGuard = (to, from, next) => {
  const token = store.state.token;
  if (!token) {
    next('/login');
  } else {
    next();
  }
};

export const adminRoutesGuard = (to, from, next) => {
  const token = store.state.token;
  if (!token) {
    next('/admin/login');
  } else {
    next();
  }
};

export const pubicRoutesGuard = (to, from, next) => {
  const token = store.state.token;
  if (token) {
    next('/dashboard');
  } else {
    next();
  }
};
