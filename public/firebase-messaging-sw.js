// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/5.5.2/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/5.5.2/firebase-messaging.js')

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
var firebaseConfig = {
  apiKey: 'AIzaSyDgn3pCzbhlGe6OAo4SJxS7U_9RQIKI6DY',
  authDomain: 'seclot-75036.firebaseapp.com',
  databaseURL: 'https://seclot-75036.firebaseio.com',
  projectId: 'seclot-75036',
  storageBucket: 'seclot-75036.appspot.com',
  messagingSenderId: '230790742160',
  appId: '1:230790742160:web:095b2e735bae75cce91815',
  measurementId: 'G-XCSJW7FJHT',
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging()
